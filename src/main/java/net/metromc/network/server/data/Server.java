package net.metromc.network.server.data;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.Getter;
import lombok.Setter;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.spigot.util.UtilJson;

import java.util.Map;
import java.util.Set;

public class Server {

    @Getter
    private final String name;
    @Getter @Setter
    private String host;
    @Getter
    private final int port;
    @Getter
    private final ServerType serverType;
    @Getter
    @Setter
    private long lastResponse;
    @Getter
    @Setter
    private Set<String> onlinePlayers;
    @Getter
    private Map<String, String> data = Maps.newHashMap();

    public Server(String name, String host, int port, ServerType serverType, long lastResponse, Set<String> onlinePlayers) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.serverType = serverType;
        this.lastResponse = lastResponse;
        this.onlinePlayers = onlinePlayers;
        data.put("startTime", String.valueOf(System.currentTimeMillis()));


        ServerDataRepository.getInstance().add(this);
    }

    public ServerState getServerState() {
        if (!data.containsKey("state")) {
            return ServerState.JOINABLE;
        }

        ServerState state = ServerState.fromString(data.get("state"));
        if (state == null) {
            state = ServerState.JOINABLE;
        }

        return state;
    }


    public long getStartTime() {
        if (!data.containsKey("startTime")) {
            return -1;
        }

        try {
            return Long.parseLong(data.get("startTime"));
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    public int getUptimeInSeconds() {
        long start = getStartTime();

        if (start == -1) {
            return -1;
        }

        return (int) (Double.valueOf(System.currentTimeMillis() - getStartTime()) / 1000D);
    }


    public int getSlots() {
        if (data.containsKey("maxplayers"))
            try {
                return Integer.parseInt(data.get("maxplayers"));
            } catch (NumberFormatException ignored) {
            }
        return 20;
    }

    public int getNumber() {
        String[] tokens = name.split("-");
        String number = tokens[tokens.length - 1];
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException ignored) {
        }
        return -1;
    }

    public String getServerStateString() {
        if (!data.containsKey("state"))
            return ServerState.JOINABLE.toString();
        return data.get("state");
    }

    public void setServerState(ServerState serverState) {
        data.put("state", serverState.toString());
    }

    public double getTimeSinceLastPingInSeconds() {
        return (System.currentTimeMillis() - lastResponse / 1000D);
    }

    public String toJsonString() {
        return this.toJsonString(lastResponse, onlinePlayers);
    }

    public String toJsonString(long response, Set<String> onlinePlayers) {
        lastResponse = response;
        this.onlinePlayers = onlinePlayers;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("host", host);
        jsonObject.addProperty("port", port);
        jsonObject.addProperty("type", serverType.getCodeName());
        jsonObject.addProperty("response", lastResponse);
        JsonArray array = new JsonArray();

        onlinePlayers.forEach(name -> array.add(name));
        jsonObject.add("players", array);

        return jsonObject.toString();
    }

    public static Server fromJsonString(String message) {
        JsonObject jsonObject = new JsonParser().parse(message).getAsJsonObject();

        String name = UtilJson.getString(jsonObject, "name");
        String host = UtilJson.getString(jsonObject, "host");
        int port = UtilJson.getInt(jsonObject, "port");
        String type = UtilJson.getString(jsonObject, "type");
        long response = UtilJson.getLong(jsonObject, "response");
        JsonArray jsonArray = jsonObject.get("players").getAsJsonArray();
        Set<String> players = Sets.newHashSet();

        jsonArray.forEach(jsonElement -> players.add(jsonElement.getAsString()));

        return new Server(name, host, port, ServerType.fromCodeName(type), response, players);
    }

}