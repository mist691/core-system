package net.metromc.network.server.data;

public enum ServerState {

    JOINABLE, OFFLINE;

    public static ServerState fromString(String name) {
        for (ServerState serverState : ServerState.values()) {
            if (name.equalsIgnoreCase(serverState.toString())) {
                return serverState;
            }
        }
        return null;
    }
}