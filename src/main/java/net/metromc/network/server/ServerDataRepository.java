package net.metromc.network.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import net.metromc.network.server.data.Server;
import net.metromc.network.server.data.ServerType;
import net.metromc.network.util.Logger;

public class ServerDataRepository {

    public static final String REGISTER_SERVER_CHANNEL = "REGISTER_SERVER";
    public static final String UNREGISTER_SERVER_CHANNEL = "REGISTER_SERVER";

    @Getter
    private final Map<String, Server> servers = Maps.newHashMap();
    private static ServerDataRepository INSTANCE;

    public Server getServer(String name) {
        return servers.get(name.toUpperCase());
    }

    public Server getServer(Integer port) {
        String server = servers.entrySet().stream().filter(stringServerEntry -> stringServerEntry.getValue().getPort() == port).findFirst().get().getKey();

        return servers.get(server);
    }

    public Server getServer(int port) {
        for (String s : servers.keySet()) {
            Server server = servers.get(s);

            if (server.getPort() == port) {
                return server;
            }
        }
        return null;
    }

    public List<Server> getServers(ServerType serverType) {
        List<Server> serverList = Lists.newArrayList();
        for (String s : servers.keySet()) {
            Server server = servers.get(s);

            if (server.getServerType().getCodeName().equals(serverType.getCodeName())) {
                serverList.add(server);
            }
        }
        return serverList;
    }

    public void remove(Server server) {
        servers.remove(server.getName().toUpperCase());
    }

    public void add(Server server) {
        if (servers.containsKey(server.getName().toUpperCase())) {
            servers.remove(server);
        }

        servers.put(server.getName().toUpperCase(), server);
    }
    public List<Server> getServersAsList() {
        List<Server> serverList = Lists.newArrayList();

        servers.forEach((s, server) -> {
            serverList.add(server);
        });

        return serverList;
    }

    public List<Server> getServersAsList(ServerType serverType) {
        List<Server> serverList = Lists.newArrayList();

        servers.forEach((s, server) -> {
            if (server.getServerType().getCodeName().equals(serverType.getCodeName())) {
                serverList.add(server);
            }
        });

        return serverList;
    }

    public static ServerDataRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ServerDataRepository();
        }

        return INSTANCE;
    }
}
