package net.metromc.network.server.redis;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.function.BiConsumer;
import lombok.Getter;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.ServerType;
import net.metromc.spigot.network.ServerRepository;

public enum ServerProperty {

    SERVER_TYPE((serverDataRepository, redisHandler) -> {
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        ServerType.values().forEach(serverType -> jsonArray.add(serverType.toJsonString()));

        jsonObject.add("types", jsonArray);
        redisHandler.publish(ServerRepository.REQUEST_FROM_CHANNEL, "SERVER_TYPE " + jsonObject.toString());
    });

    @Getter
    private final BiConsumer<ServerDataRepository, RedisHandler> consumer;

    ServerProperty(BiConsumer<ServerDataRepository, RedisHandler> consumer) {
        this.consumer = consumer;
    }
}
