package net.metromc.network.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jline.console.ConsoleReader;
import net.metromc.network.command.commands.CommandCreate;
import net.metromc.network.command.commands.CommandDestroy;
import net.metromc.network.command.commands.CommandHelp;
import net.metromc.network.command.commands.CommandLocate;
import net.metromc.network.command.commands.CommandPlayers;
import net.metromc.network.command.commands.CommandServer;
import net.metromc.network.command.commands.CommandServers;
import net.metromc.network.command.commands.CommandStop;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.monitor.ServerMonitor;
import net.metromc.network.server.redis.RedisHandler;
import net.metromc.network.util.Logger;
import net.metromc.network.util.UtilString;

public class CommandCenter extends Thread {

    private final List<Command> commands = new ArrayList<>();
    public static final String PROMPT = "Core-System> ";


    public CommandCenter(ServerDataRepository serverDataRepository, ServerMonitor serverMonitor, RedisHandler redisHandler) {
        super("Command Center");

        addCommand(new CommandServers());
        addCommand(new CommandServer());
        addCommand(new CommandStop());
        addCommand(new CommandCreate(serverMonitor));
        addCommand(new CommandPlayers(serverDataRepository));
        addCommand(new CommandLocate(serverDataRepository));
        addCommand(new CommandDestroy(ServerMonitor.getInstance(serverDataRepository, redisHandler)));
        addCommand(new CommandHelp(this));

        start();
    }

    public List<Command> getCommands() {
        return commands;
    }

    private void addCommand(Command command) {
        commands.add(command);
    }

    @Override
    public void run() {
        ConsoleReader console = Logger.getInstance().getConsoleReader();
        try {
            listener: while (true) {
                String line = console.readLine(PROMPT), raw = new String(line);

                if (line == null) {
                    continue;
                }

                line = UtilString.removeRecurringSpaces(line.trim());

                if (line.isEmpty()) {
                    continue;
                }

                List<String> arguments = new ArrayList<>(Arrays.asList(line.split(" ")));
                String aliasUsed = arguments.remove(0);

                if (aliasUsed.startsWith("/")) {
                    aliasUsed = aliasUsed.substring(1);
                }

                Logger.getInstance().setCommand(true);

                for (Command command : commands) {
                    for (String alias : command.getAliases()) {
                        if (alias.equalsIgnoreCase(aliasUsed)) {
                            if (aliasUsed == null || arguments == null) {
                                return;
                            }

                            command.execute(aliasUsed, arguments.toArray(new String[arguments.size()]));
                            command.executeRaw(aliasUsed, raw, arguments.toArray(new String[arguments.size()]));
                            continue listener;
                        }
                    }
                }
                Logger.log("Could not find command '" + aliasUsed + "', type help for a list of commands.");
            }
        } catch (IOException ignored) {
        }
    }
}
