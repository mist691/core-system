package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.util.Logger;
import net.metromc.spigot.util.UtilTime;

public class CommandServer extends Command {

    public CommandServer() {
        super("Server", "display a servers information", "server");
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        if (args.length == 0) {
            Logger.log("You need to supply a servers name");
            return;
        }

        String name = args[0];
        Server server = ServerDataRepository.getInstance().getServer(name);

        if (server == null) {
            Logger.log("Their is no server named " + name);
            return;
        }

        Logger.log("Server " + server.getName() + " " + " type: " + server.getServerType().getDisplayName() + " host: " + server.getHost() + " port: " + server.getPort() + " lastResponse: " + UtilTime.getTimeSinceLastResponseInSeconds(server.getLastResponse()) + " seconds players: (" + server.getOnlinePlayers().size() + ")");
    }
}
