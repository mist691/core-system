package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.command.CommandCenter;
import net.metromc.network.util.Logger;

public class CommandHelp extends Command {

    private final CommandCenter commandCenter;

    public CommandHelp(CommandCenter commandCenter) {
        super("help", "Gives command information", "help");
        this.commandCenter = commandCenter;
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        Logger.log("Command List:");
        for (Command command : commandCenter.getCommands()) {
            String usage = "> " + command.getName();
            for (String s : command.getUsage())
                usage += " " + s;
            usage += " - " + command.getDescription();
            Logger.log(usage);
        }
    }
}
