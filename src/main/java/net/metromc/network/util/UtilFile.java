package net.metromc.network.util;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

public class UtilFile {

	private static UtilFile instance;
	private static byte[] SKIP_BYTE_BUFFER;

	public static File copyFromResourcesFolder(String resource, File destination) {
		InputStream inputStream = getInstance().getClass().getClassLoader().getResourceAsStream(resource);
		if (!destination.isDirectory())
			return null;
		String resourceName = resource.split(File.separator)[resource.split(File.separator).length - 1];
		try {
			File file = new File(destination.getPath() + File.separator + resourceName);
			file.createNewFile();
			copyInputStreamToFile(inputStream, file);
			inputStream.close();
			return file;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public static void makeFolderIfNotExist(File folder) {
		if (!folder.isDirectory())
			folder.mkdir();
	}

	private static UtilFile getInstance() {
		if (instance == null)
			instance = new UtilFile();
		return instance;
	}

	/* Apache IOUtils */

	public static void copyInputStreamToFile(InputStream var0, File var1) throws IOException {
		try {
			copyToFile(var0, var1);
		} finally {
			closeQuietly(var0);
		}

	}

	public static  void copyToFile(InputStream var0, File var1) throws IOException {
		FileOutputStream var2 = openOutputStream(var1, false);

		try {
			copy(var0, var2);
			var2.close();
		} finally {
			closeQuietly(var2);
		}

	}

	public static  FileOutputStream openOutputStream(File var0, boolean var1) throws IOException {
		if (var0.exists()) {
			if (var0.isDirectory()) {
				throw new IOException("File '" + var0 + "' exists but is a directory");
			}

			if (!var0.canWrite()) {
				throw new IOException("File '" + var0 + "' cannot be written to");
			}
		} else {
			File var2 = var0.getParentFile();
			if (var2 != null && !var2.mkdirs() && !var2.isDirectory()) {
				throw new IOException("Directory '" + var2 + "' could not be created");
			}
		}

		return new FileOutputStream(var0, var1);
	}

	public static void closeQuietly(Reader var0) {
		closeQuietly((Closeable)var0);
	}

	public static void closeQuietly(Writer var0) {
		closeQuietly((Closeable)var0);
	}

	public static void closeQuietly(InputStream var0) {
		closeQuietly((Closeable)var0);
	}

	public static void closeQuietly(OutputStream var0) {
		closeQuietly((Closeable)var0);
	}

	public static void closeQuietly(Closeable var0) {
		try {
			if (var0 != null) {
				var0.close();
			}
		} catch (IOException var2) {
		}

	}

	public static int copy(InputStream var0, OutputStream var1) throws IOException {
		long var2 = copyLarge(var0, var1);
		return var2 > 2147483647L ? -1 : (int)var2;
	}

	public static long copy(InputStream var0, OutputStream var1, int var2) throws IOException {
		return copyLarge(var0, var1, new byte[var2]);
	}

	public static long copyLarge(InputStream var0, OutputStream var1) throws IOException {
		return copy(var0, var1, 4096);
	}

	public static long copyLarge(InputStream var0, OutputStream var1, byte[] var2) throws IOException {
		long var3;
		int var5;
		for(var3 = 0L; -1 != (var5 = var0.read(var2)); var3 += (long)var5) {
			var1.write(var2, 0, var5);
		}

		return var3;
	}

	public static long copyLarge(InputStream var0, OutputStream var1, long var2, long var4) throws IOException {
		return copyLarge(var0, var1, var2, var4, new byte[4096]);
	}

	public static long copyLarge(InputStream var0, OutputStream var1, long var2, long var4, byte[] var6) throws IOException {
		if (var2 > 0L) {
			skipFully(var0, var2);
		}

		if (var4 == 0L) {
			return 0L;
		} else {
			int var7 = var6.length;
			int var8 = var7;
			if (var4 > 0L && var4 < (long)var7) {
				var8 = (int)var4;
			}

			long var9 = 0L;

			int var11;
			while(var8 > 0 && -1 != (var11 = var0.read(var6, 0, var8))) {
				var1.write(var6, 0, var11);
				var9 += (long)var11;
				if (var4 > 0L) {
					var8 = (int)Math.min(var4 - var9, (long)var7);
				}
			}

			return var9;
		}
	}

	public static void skipFully(InputStream var0, long var1) throws IOException {
		if (var1 < 0L) {
			throw new IllegalArgumentException("Bytes to skip must not be negative: " + var1);
		} else {
			long var3 = skip(var0, var1);
			if (var3 != var1) {
				throw new EOFException("Bytes to skip: " + var1 + " actual: " + var3);
			}
		}
	}

	public static long skip(InputStream var0, long var1) throws IOException {
		if (var1 < 0L) {
			throw new IllegalArgumentException("Skip count must be non-negative, actual: " + var1);
		} else {
			if (SKIP_BYTE_BUFFER == null) {
				SKIP_BYTE_BUFFER = new byte[2048];
			}

			long var3;
			long var5;
			for(var3 = var1; var3 > 0L; var3 -= var5) {
				var5 = (long)var0.read(SKIP_BYTE_BUFFER, 0, (int)Math.min(var3, 2048L));
				if (var5 < 0L) {
					break;
				}
			}

			return var1 - var3;
		}
	}

}