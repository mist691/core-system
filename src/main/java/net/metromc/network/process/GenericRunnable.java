package net.metromc.network.process;

public interface GenericRunnable<T> {

    void run(T t);
}
