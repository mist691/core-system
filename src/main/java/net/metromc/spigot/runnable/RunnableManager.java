package net.metromc.spigot.runnable;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.function.Consumer;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import org.bukkit.Bukkit;

public class RunnableManager extends Module {

    private final Map<String, MetroRunnable> runnables = Maps.newConcurrentMap();

    public RunnableManager(MetroMC plugin) {
        super(plugin, "RunnableManager");
    }

    public void runTask(String name, Consumer<MetroMC> run) {
        Bukkit.getScheduler().runTask(getPlugin(), createRunnable(name, run));
    }

    public void runTaskAsynchronously(String name, Consumer<MetroMC> run) {
        Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), createRunnable(name, run));
    }

    public void runTaskLater(String name, Consumer<MetroMC> run, long time) {
        Bukkit.getScheduler().runTaskLater(getPlugin(), createRunnable(name, run), time);
    }

    public void runTaskLaterAsynchronously(String name, Consumer<MetroMC> run, long time) {
        Bukkit.getScheduler().runTaskLaterAsynchronously(getPlugin(), createRunnable(name, run), time);
    }

    public void runTaskTimer(String name, Consumer<MetroMC> run, long delay, long period) {
        Bukkit.getScheduler().runTaskTimer(getPlugin(), createRunnable(name, run), delay, period);
    }

    public void runTaskTimerAsynchronously(String name, Consumer<MetroMC> run, long delay, long period) {
        Bukkit.getScheduler().runTaskTimerAsynchronously(getPlugin(), createRunnable(name, run), delay, period);
    }

    public void updateTime(String name, long delay, long period) {
        MetroRunnable MetroRunnable = runnables.get(name);

        if (MetroRunnable == null) {
            return;
        }

        MetroRunnable newRunnable = MetroRunnable.clone();

        runnables.remove(name);

        runTaskTimer(name, newRunnable.getRun(), delay, period);

        return;
    }

    public MetroRunnable createRunnable(String name, Consumer<MetroMC> run) {
        MetroRunnable runnable = new MetroRunnable(getPlugin(), this, name, run);

        runnables.put(name, runnable);

        return runnable;
    }

    public Map<String, MetroRunnable> getRunnables() {
        return runnables;
    }

}