package net.metromc.spigot.scoreboard;

import java.util.Arrays;
import java.util.List;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.Domain;
import org.bukkit.ChatColor;

public class DefaultScoreboard implements ScoreboardPolicy {

    @Override
    public List<String> getSidebar(User user) {
        return Arrays.asList(
                " ",
                ChatColor.WHITE + "Rank: " + (user.getRank().equals(Rank.NONE) ? ChatColor.GRAY + "None" : user.getRank().getPrefix()),
                "  ",
                ChatColor.YELLOW + Domain.getDomain().getWebsite());
    }

    @Override
    public String getPrefix(User perspective, User subject) {
        if (subject.getRank().getId() == Rank.NONE.getId()) {
            return subject.getRank().getPrefix();
        }

        return subject.getRank().getPrefix() + " ";
    }

    @Override
    public String getSuffix(User perspective, User subject) {
        return null;
    }

    @Override
    public String getUndername(User user) {
        return null;
    }

    @Override
    public int getUndernameScore(User perspective, User subject) {
        return 0;
    }

    @Override
    public String getTablist(User user) {
        return null;
    }

    @Override
    public int getTablistScore(User perspective, User subject) {
        return 0;
    }
}

