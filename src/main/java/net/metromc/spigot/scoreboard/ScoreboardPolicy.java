package net.metromc.spigot.scoreboard;

import java.util.List;
import net.metromc.spigot.user.object.User;

public interface ScoreboardPolicy {

	List<String> getSidebar(User user);

	String getPrefix(User perspective, User subject);

	String getSuffix(User perspective, User subject);

	String getUndername(User User);

	int getUndernameScore(User perspective, User subject);

	String getTablist(User User);

	int getTablistScore(User perspective, User subject);
}
