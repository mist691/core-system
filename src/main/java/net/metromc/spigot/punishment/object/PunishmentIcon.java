package net.metromc.spigot.punishment.object;

import java.util.List;
import java.util.concurrent.TimeUnit;
import lombok.Getter;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.util.ItemBuilder;
import net.metromc.spigot.util.UtilString;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum PunishmentIcon {

    GENERAL_ONE(PunishmentCategory.GENERAL, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 5).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.MINUTES.toMillis(30), UtilString.split(ChatColor.GRAY + "Example first degree general ban", 40)),
    GENERAL_TWO(PunishmentCategory.GENERAL, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,4).setName(ChatColor.GREEN + "SECOND DEGREE").build(), TimeUnit.DAYS.toMillis(1), UtilString.split(ChatColor.GRAY + "Example second degree general ban", 40)),
    GENERAL_THREE(PunishmentCategory.GENERAL, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 1).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.DAYS.toMillis(7), UtilString.split(ChatColor.GRAY + "Example third degree general ban", 40)),
    GENERAL_FOUR(PunishmentCategory.GENERAL, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 14).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.DAYS.toMillis(7), UtilString.split(ChatColor.GRAY + "Example fourth degree general ban", 40)),

    CHAT_ONE(PunishmentCategory.CHAT, PunishmentType.MUTE, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 5).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.MINUTES.toMillis(30), UtilString.split(ChatColor.GRAY + "Example first degree mute", 40)),
    CHAT_TWO(PunishmentCategory.CHAT, PunishmentType.MUTE, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,4).setName(ChatColor.GREEN + "SECOND DEGREE").build(), TimeUnit.DAYS.toMillis(1), UtilString.split(ChatColor.GRAY + "Example second degree mute", 40)),
    CHAT_THREE(PunishmentCategory.CHAT, PunishmentType.MUTE, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 1).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.DAYS.toMillis(7), UtilString.split(ChatColor.GRAY + "Example third degree mute", 40)),
    CHAT_FOUR(PunishmentCategory.CHAT, PunishmentType.MUTE, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 14).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.DAYS.toMillis(7), UtilString.split(ChatColor.GRAY + "Example fourth  degree mute", 40)),

    HACKING_ONE(PunishmentCategory.HACKING, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 5).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.MINUTES.toMillis(30), UtilString.split(ChatColor.GRAY + "Example first degree hacking ban", 40)),
    HACKING_TWO(PunishmentCategory.HACKING, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1,4).setName(ChatColor.GREEN + "SECOND DEGREE").build(), TimeUnit.DAYS.toMillis(1), UtilString.split(ChatColor.GRAY + "Example second degree hacking ban", 40)),
    HACKING_THREE(PunishmentCategory.HACKING, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 1).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.DAYS.toMillis(7), UtilString.split(ChatColor.GRAY + "Example third degree hacking ban", 40)),
    HACKING_FOUR(PunishmentCategory.HACKING, PunishmentType.BAN, Rank.HELPER, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 14).setName(ChatColor.GREEN + "FIRST DEGREE").build(), TimeUnit.DAYS.toMillis(7), UtilString.split(ChatColor.GRAY + "Example fourth  degree hacking ban", 40)),

    WARNING(PunishmentCategory.MISC, PunishmentType.WARNING, Rank.HELPER, new ItemBuilder(Material.PAPER).setName("Warning").build(), -1, UtilString.split(ChatColor.GRAY + "Give a player a warning", 40)),
    PERM_MUTE(PunishmentCategory.PERM_PUNISHMENTS, PunishmentType.PERM_MUTE, Rank.ADMIN, new ItemBuilder(Material.BOOK_AND_QUILL).setName("Perm mute").build(), -1, UtilString.split(ChatColor.GRAY + "Give a player a perm mute", 40)),
    PERM_BAN(PunishmentCategory.PERM_PUNISHMENTS, PunishmentType.PERM_BAN, Rank.ADMIN, new ItemBuilder(Material.REDSTONE_BLOCK).setName("Perm ban").build(), -1, UtilString.split(ChatColor.GRAY + "Give a player a perm ban", 40)),
    ;

    @Getter
    private final PunishmentCategory category;
    @Getter
    private final PunishmentType type;
    @Getter
    private final Rank allowedRank;
    @Getter
    private final ItemStack icon;
    @Getter
    private final long punishTime;
    @Getter
    private final List<String> description;

    PunishmentIcon(PunishmentCategory category, PunishmentType type, Rank allowedRank, ItemStack icon, long punishTime, List<String> description) {
        this.category = category;
        this.type = type;
        this.allowedRank = allowedRank;
        this.icon = icon;
        this.punishTime = punishTime;
        this.description = description;
    }
}
