package net.metromc.spigot.punishment.command;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.punishment.ui.PunishmentMenu;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.Permission;
import org.bukkit.entity.Player;

public class CommandPunish extends Command {

    public CommandPunish(MetroMC plugin) {
        super(plugin, Rank.HELPER, "Open the punishment menu", "pu", "punish", "ban", "mute", "kick", "warn");
        setUsage("<name>", "<reason>");
    }

    @Override
    public void execute(Player player, String[] args) {
        if (!checkArgs(args)) {
            sendUsageMessage(player);
            return;
        }

        getPlugin().getRunnableManager().runTaskAsynchronously("Punishment Task", metroMC -> {
            User punisher = getPlugin().getUserManager().getUser(player);
            User punished = getPlugin().getUserManager().getUser(args[0], true);
            String reason = "";

            if (punished == null) {
                player.sendMessage(C.ERROR_COLOUR + "This player doesn't exist in our database");
                return;
            }

            for (int i = 1; i < args.length; i++) {
                reason += args[i] + " ";
            }

            if (!Permission.allowed(punisher, punished.getRank())) {
                return;
            }

            new PunishmentMenu(getPlugin(), punisher, punished, reason).open(player);
        });
    }
}
