package net.metromc.spigot.punishment;

import java.util.UUID;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.punishment.object.PunishmentType;
import net.metromc.spigot.redis.RedisMessageRecieveEvent;
import net.metromc.spigot.user.object.User;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PunishmentHandler implements Listener {

    private static final String PUNISHMENT_CHANNEL = "PUNISHMENT";

    private final MetroMC plugin;

    public PunishmentHandler(MetroMC plugin) {
        this.plugin = plugin;

        plugin.getRedisHandler().registerChannel(PUNISHMENT_CHANNEL);
    }

    public void handlePunishment(UUID uuid, Punishment punishment, String formattedReason) {
        plugin.getRedisHandler().publish(PUNISHMENT_CHANNEL, uuid.toString() + "//" + punishment.toDocument().toJson() + "//" + formattedReason);
    }

    @EventHandler
    public void on(RedisMessageRecieveEvent e) {
        if (!e.getChannel().equals(PUNISHMENT_CHANNEL)) {
            return;
        }

        String[] data = e.getMessage().split("//");

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getUniqueId().equals(UUID.fromString(data[0]))) {
                User user = plugin.getUserManager().getUser(player);
                Punishment punishment = Punishment.fromDocument(Document.parse(data[1]));

                user.addPunishment(punishment);
                user.update();

                if (punishment.getType() == PunishmentType.BAN || punishment.getType() == PunishmentType.KICK) {
                    plugin.getRunnableManager().runTask("Kick task", metroMC -> player.kickPlayer(data[2]));
                }
            }
        } // else?
    }
}
