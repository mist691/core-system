package net.metromc.spigot.punishment.ui;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.gui.Gui;
import net.metromc.spigot.gui.GuiElement;
import net.metromc.spigot.gui.context.GuiSize;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.punishment.object.PunishmentCategory;
import net.metromc.spigot.punishment.object.PunishmentIcon;
import net.metromc.spigot.punishment.object.PunishmentType;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.ItemBuilder;
import net.metromc.spigot.util.Permission;
import net.metromc.spigot.util.UtilItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class PunishmentMenu extends Gui {

    public PunishmentMenu(MetroMC plugin, User punisher, User punished, String reason) {
        super(plugin, "Punish - " + punished.getName() , GuiSize.SIX_ROWS);

        //Grid
        for (int i = 0; i < GuiSize.SIX_ROWS.getSlots(); i++) {
            if (i >= 9 && i <= 17 || (i - 1) % 9 == 0) {
                addElement(i, new GuiElement() {
                    @Override
                    public ItemStack getIcon(Player player) {
                        return new ItemBuilder(Material.STAINED_GLASS_PANE, 1,7).build();
                    }

                    @Override
                    public void click(Player player, ClickType clickType) {

                    }
                });
            }
        }

        //Skull
        addElement(0, new GuiElement() {
            @Override
            public ItemStack getIcon(Player player) {
                return UtilItem.getPlayerSkull(player.getName(), player.getName());
            }

            @Override
            public void click(Player player, ClickType clickType) {
            }
        });

        //Punish history button
        addElement(18, new GuiElement() {
            @Override
            public ItemStack getIcon(Player player) {
                return new ItemBuilder(Material.BOOKSHELF).setName("Punish history").setLore("Check " + punished.getName() + "'s punish history").build();
            }

            @Override
            public void click(Player player, ClickType clickType) {
                new PunishHistoryMenu(plugin, punished).open(player);
            }
        });

        //Category icons
        for (PunishmentCategory punishmentCategory : PunishmentCategory.values()) {
            if (!punishmentCategory.isDisplayIcon()) {
                continue;
            }

            addElement(punishmentCategory.getPos(), new GuiElement() {
                @Override
                public ItemStack getIcon(Player player) {
                    return punishmentCategory.getItemStack();
                }

                @Override
                public void click(Player player, ClickType clickType) {
                    String desc = "";

                    for (String s : punishmentCategory.getDesc()) {
                        desc += s + " ";
                    }

                    player.sendMessage(C.ERROR_COLOUR + punishmentCategory.getName() + ": " + C.MESSAGE_HIGHLIGHT + desc);
                }
            });
        }

        //Punishments buttons
        for (PunishmentIcon icon : PunishmentIcon.values()) {
            addElement(icon.getCategory().getFirstFreeSlot(this), new GuiElement() {
                @Override
                public ItemStack getIcon(Player player) {
                    ItemBuilder itemBuilder = new ItemBuilder(icon.getIcon().getType(), icon.getIcon().getAmount(), icon.getIcon().getDurability());

                    itemBuilder.setName(icon.getIcon().getItemMeta().getDisplayName());
                    itemBuilder.setLore(icon.getDescription());
                    itemBuilder.addLore("");
                    itemBuilder.addLore(ChatColor.GRAY + "Rank needed: " + icon.getAllowedRank().getPrefix());

                    return itemBuilder.build();
                }

                @Override
                public void click(Player player, ClickType clickType) {
                    if(Permission.allowed(punisher, icon.getAllowedRank(), false)) {
                        boolean addTime = true; // Should it send time for punishment in staffchat
                        Punishment punishment = new Punishment(punisher.getUuid(), punished.getUuid(), reason, icon, icon.getType(), System.currentTimeMillis(), icon.getPunishTime(), true);

                        if(icon.getType().equals(PunishmentType.KICK) || icon.getType().equals(PunishmentType.WARNING)) {
                            punishment.setActive(false);
                            addTime = false;
                        }

                        plugin.getPunishmentManager().punish(punished, punisher, punishment);

                        //TODO send message in staff chat

                        close(player);
                    }
                }
            });
        }
    }
}
