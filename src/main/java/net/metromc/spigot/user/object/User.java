package net.metromc.spigot.user.object;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.UUID;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.punishment.object.PunishmentIcon;
import net.metromc.spigot.punishment.object.PunishmentType;
import net.metromc.spigot.story.object.Story;
import net.metromc.spigot.user.settings.Setting;
import net.metromc.spigot.user.settings.event.ToggleSettingEvent;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.UtilString;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class User {

    private final MetroMC plugin;
    private final Document document;

    public User(MetroMC plugin, Document document) {
        this.plugin = plugin;
        this.document = document;
    }

    public User(MetroMC plugin, UUID uuid, String name, String ip) {
        this.plugin = plugin;

        Document document = new Document();

        document.put("uuid", uuid.toString());
        document.put("name", name.toLowerCase());
        document.put("rank", Rank.NONE.getCodeName());
        document.put("ip", ip);

        this.document = document;

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> plugin.getUserManager().getCollection().insertOne(document));
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(getUuid());
    }

    public UUID getUuid() {
        return UUID.fromString(document.getString("uuid"));
    }

    public String getName() {
        return document.getString("name");
    }

    public Rank getRank() {
        return Rank.fromName(document.getString("rank"));
    }

    public String getIp() {
        return document.getString("ip");
    }

    public List<Punishment> getPunishments() {
        List<Punishment> punishments = Lists.newArrayList();

        if (document.get("punishments") != null) {
            List<Document> documentes = (List<Document>) document.get("punishments");

            documentes.forEach(document -> punishments.add(new Punishment(UUID.fromString(document.getString("punisher")), UUID.fromString(document.getString("punished")),
                    document.getString("reason"), PunishmentIcon.valueOf(document.getString("icon")), PunishmentType.valueOf(document.getString("type")), document.getLong("issued"),
                    document.getLong("expire"), document.getBoolean("active"), (document.containsKey("deactivator") ? UUID.fromString(document.getString("deactivator")) : null))));
        }

        return punishments;
    }


    public void addPunishment(Punishment punishment) {
        List<Document> documents = Lists.newArrayList();
        documents.add(punishment.toDocument());

        for (Punishment punish : getPunishments()) {
            documents.add(punish.toDocument());
        }

        put("punishments", documents);
    }

    public void updatePunishments(List<Punishment> punishments) {
        List<Document> documents = Lists.newArrayList();

        for (Punishment punish : punishments) {
            documents.add(punish.toDocument());
        }

        put("punishments", documents);
    }

    public boolean getSetting(Setting setting) {
        putIfAbsent("settings", Lists.newArrayList());

        List<Document> documents = (List<Document>) document.get("settings");

        for (Document document : documents) {
            if (document.getString("name").equals(setting.getDatabaseString())) {
                return document.getBoolean("active");
            }
        }

        return setting.getDefaultValue();
    }

    public void toggleSetting(Setting setting) {
        putIfAbsent("settings", Lists.newArrayList());

        boolean active = setting.getDefaultValue();

        boolean found = false;
        List<Document> documents = (List<Document>) document.get("settings");

        for (Document document : documents) {
            if (document.getString("name").equals(setting.getDatabaseString())) {
                active = !document.getBoolean("active");
                document.put("active", active);
                found = true;
            }
        }

        if (!found) {
            documents.add(new Document().append("name", setting.getDatabaseString()).append("active", setting.getDefaultValue()));
        }

        put("settings", documents);

        getPlayer().sendMessage(C.CHAT_COLOUR + "Toggled " + C.MESSAGE_HIGHLIGHT + UtilString.capitaliseFirstCharacter(setting.getName()));
        Bukkit.getServer().getPluginManager().callEvent(new ToggleSettingEvent(this, setting, active));
    }

    public void put(String key, Object value) {
        document.put(key, value);
        update();
    }

    public Story getCurrentStory() {
        return plugin.getStoryManager().getStoryFromPlayer(this);
    }

    public void putIfAbsent(String key, Object value) {
        document.putIfAbsent(key, value);
    }

    public boolean contains(String key) {
        return document.containsKey(key);
    }

    public Object get(String key) {
        return document.get(key);
    }

    public String getString(String key) {
        return document.getString(key);
    }

    public int getInteger(String key) {
        return document.getInteger(key);
    }

    public long getLong(String key) {
        return document.getLong(key);
    }

    public void update() {
        plugin.getUserManager().update(this, true);
    }

    public Document getDocument() {
        return document;
    }
}
