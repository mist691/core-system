package net.metromc.spigot.user.chat;

import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.punishment.object.PunishmentType;
import net.metromc.spigot.user.chat.event.PlayerChatEvent;
import net.metromc.spigot.user.chat.event.PlayerRecieveChatEvent;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.UtilServer;
import net.metromc.spigot.util.UtilString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatManager extends Module {

    public ChatManager(MetroMC plugin) {
        super(plugin, "Chat Manager");
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        Player player = e.getPlayer();
        User user = getPlugin().getUserManager().getUser(player);
        String message = e.getMessage();

        if (!user.getPunishments().isEmpty()) {
            List<Punishment> punishments = user.getPunishments();

            for (Punishment punishment : punishments) {
                if (punishment.getType().equals(PunishmentType.MUTE) || punishment.getType().equals(PunishmentType.PERM_MUTE)) {
                    if (punishment.getExpireTime() != -1 && punishment.getIssuedTime() + punishment.getExpireTime() <= System.currentTimeMillis()) {
                        punishment.setActive(false);
                        user.updatePunishments(punishments);
                        continue;
                    }

                    if (punishment.isActive()) {
                        player.sendMessage(getPlugin().getPunishmentManager().getMessage(getPlugin().getUserManager().getUser(punishment.getPunisher(), true), punishment));
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }

        if (user.getRank().getId() >= Rank.ADMIN.getId()) {
            message = ChatColor.translateAlternateColorCodes('&', message);
        }

        PlayerChatEvent chatEvent = new PlayerChatEvent(player, message);
        Bukkit.getPluginManager().callEvent(chatEvent);

        if (chatEvent.getPrefix() != null) {
            chatEvent.setPrefix(user.getRank().getPrefix() + " ");
        } else {
            chatEvent.setPrefix("");
        }

        if (chatEvent.isCancelled()) {
            if (chatEvent.getCancelReason() != null) {
                player.sendMessage(chatEvent.getCancelReason());
                return;
            }
        }

        ChatColor textColour = (getPlugin().getUserManager().getUser(player).getRank().getTextColor());
        ChatColor rankColour = getPlugin().getUserManager().getUser(player).getRank().getColor();

        if (chatEvent.getLevelPrefix() != null) {
            String level = chatEvent.getLevelPrefix();

            UtilServer.broadcastMessage(ChatColor.GRAY + level + ChatColor.GRAY + " | " + rankColour + chatEvent.getPrefix() + rankColour + player.getName() + chatEvent.getSuffix() + textColour + " " + chatEvent.getMsg(), PlayerRecieveChatEvent.MessageType.PLAYER_MESSAGE, player);
        } else {
            UtilServer.broadcastMessage(rankColour + chatEvent.getPrefix() + player.getName() + " " + chatEvent.getSuffix()
                    + textColour + ": " + chatEvent.getMsg(), PlayerRecieveChatEvent.MessageType.PLAYER_MESSAGE, player);
        }
    }

    @EventHandler
    public void onRecieve(PlayerRecieveChatEvent e) {
        if (e.getSender() == null || e.getPlayer() == null) {
            return;
        }

        if (e.getSender().getName().equals(e.getPlayer().getName())) {
            return;
        }

        if (!e.getMessageType().equals(PlayerRecieveChatEvent.MessageType.PLAYER_MESSAGE)) {
            return;
        }

        User sender = getPlugin().getUserManager().getUser(e.getSender());
        User reciever = getPlugin().getUserManager().getUser(e.getPlayer());

        e.setMsg(UtilString.replaceAllIgnoreCase(e.getMsg(), reciever.getName(), C.CHAT_HIGHLIGHT_COLOUR + reciever.getName() + sender.getRank().getTextColor()));
    }
}
