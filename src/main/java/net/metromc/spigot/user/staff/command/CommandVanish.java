package net.metromc.spigot.user.staff.command;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.object.Rank;
import org.bukkit.entity.Player;

public class CommandVanish extends Command {

    public CommandVanish(MetroMC plugin) {
        super(plugin, Rank.getFirstStaffRank(), "Toggle the vanish setting", "vanish", "v");
    }

    @Override
    public void execute(Player player, String[] args) {
        getPlugin().getUserManager().getUser(player).toggleSetting(getPlugin().getSettingsManager().getSetting("vanish"));
    }
}
