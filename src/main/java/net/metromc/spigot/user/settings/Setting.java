package net.metromc.spigot.user.settings;

import net.metromc.spigot.user.object.Rank;
import org.bukkit.Material;

public class Setting {

    private final String name;
    private final Boolean defaultValue;
    private final Rank rank;
    private final Material displayMaterial;

    Setting(String name, Rank rank, Material displayMaterial, Boolean defaultValue) {
        this.name = name;
        this.rank = rank;
        this.displayMaterial = displayMaterial;
        this.defaultValue = defaultValue;
    }

    public String getName() {
        return name;
    }

    public Rank getRank() {
        return rank;
    }

    public Material getDisplayMaterial() {
        return displayMaterial;
    }

    public Boolean getDefaultValue() {
        return defaultValue;
    }

    public String getDatabaseString() {
        return name.toUpperCase().replaceAll(" ", "_");
    }

    public boolean equals(String name) {
        return this.name.toUpperCase().equals(name.toUpperCase());
    }
}