package net.metromc.spigot.user.settings.ui;

import com.google.common.collect.Lists;
import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.gui.Gui;
import net.metromc.spigot.gui.GuiElement;
import net.metromc.spigot.gui.context.GuiSize;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.user.settings.Setting;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.ItemBuilder;
import net.metromc.spigot.util.UtilString;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class SettingsMenu extends Gui {

    private final List<Setting> addedSettings = Lists.newArrayList();

    public SettingsMenu(MetroMC plugin) {
        super(plugin, "Settings", GuiSize.SIX_ROWS);

        for (Setting setting : plugin.getSettingsManager().getSettings()) {
            for (int i = 0; i < 53; i++) {
                if (i > 8 && i % 2 == 1) {
                    if (this.getElements().containsKey(i) || addedSettings.contains(setting) || i + 9 >= 54) {
                        continue;
                    }

                    addedSettings.add(setting);
                    addElement(i, new GuiElement() {
                        @Override
                        public ItemStack getIcon(Player player) {
                            return new ItemBuilder(setting.getDisplayMaterial()).setName(C.TITLE_COLOR + UtilString.capitaliseFirstCharacter(setting.getName())).addLore("", C.SUBTITLE_COLOR + "Click to toggle").build();
                        }

                        @Override
                        public void click(Player player, ClickType clickType) {
                            plugin.getUserManager().getUser(player).toggleSetting(setting);
                            open(player);
                        }
                    });

                    addElement(i + 9, new GuiElement() {
                        @Override
                        public ItemStack getIcon(Player player) {
                            return plugin.getUserManager().getUser(player).getSetting(setting) ? new ItemBuilder(Material.INK_SACK, 1, 10).setName(ChatColor.GREEN + "Active").build() : new ItemBuilder(Material.INK_SACK, 1, 1).setName(ChatColor.RED + "Inactive").build();
                        }

                        @Override
                        public void click(Player player, ClickType clickType) {
                            plugin.getUserManager().getUser(player).toggleSetting(setting);
                            open(player);
                        }
                    });
                }
            }
        }
    }
}
