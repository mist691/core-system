package net.metromc.spigot.util;

@FunctionalInterface
public interface Callback<T> {

    void result(T t);

}
