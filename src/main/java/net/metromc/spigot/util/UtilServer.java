package net.metromc.spigot.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import net.metromc.spigot.user.chat.event.PlayerRecieveChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class UtilServer {

    public static void broadcastMessage(String msg, PlayerRecieveChatEvent.MessageType type, Player sender) {
        for (Player pl : Bukkit.getOnlinePlayers()) {
            UtilPlayer.message(pl, msg, type, sender);
        }
    }

    public static String getExternalIP() {
        D.d("Called getExternalIP");
        BufferedReader in = null;
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            return in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
