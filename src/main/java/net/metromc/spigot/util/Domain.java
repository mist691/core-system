package net.metromc.spigot.util;

import org.bukkit.ChatColor;

public enum Domain {

    METRO_MC("MetroMC", "MMC", "metromc.net", "store.metromc.net", ChatColor.AQUA);

    private volatile String fullName;
    private volatile String shortName;
    private volatile String website;
    private volatile String store;
    private volatile ChatColor serverColor;

    Domain(String fullName, String shortName, String website, String store, ChatColor serverColor) {
        this.fullName = fullName;
        this.shortName = shortName;
        this.website = website;
        this.store = store;
        this.serverColor = serverColor;
    }

    public static Domain getDomain() {
        return Domain.valueOf(System.getProperties().getProperty("server"));
    }

    public String getFullName() {
        return fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getWebsite() {
        return "www." + website;
    }

    public String getStore() {
        return store;
    }

    public ChatColor getServerColor() {
        return serverColor;
    }
}
