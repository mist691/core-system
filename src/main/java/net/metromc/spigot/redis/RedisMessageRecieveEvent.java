package net.metromc.spigot.redis;

import net.metromc.spigot.util.MetroEvent;

public class RedisMessageRecieveEvent extends MetroEvent {

    private final String channel;
    private final String message;

    public RedisMessageRecieveEvent(String channel, String message) {
        this.channel = channel;
        this.message = message;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }
}
