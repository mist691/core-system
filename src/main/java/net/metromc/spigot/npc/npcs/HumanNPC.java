package net.metromc.spigot.npc.npcs;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.npc.NPC;
import net.metromc.spigot.util.UtilPlayer;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.MinecraftServer;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_12_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_12_R1.PlayerInteractManager;
import net.minecraft.server.v1_12_R1.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Player;

public class HumanNPC extends NPC {

    @Getter
    private final String texture;
    @Getter
    private final String signature;

    private final GameProfile gameProfile;
    private final EntityPlayer entityPlayer;

    public HumanNPC(MetroMC plugin, Location location, List<String> name, String texture, String signature) {
        super(plugin, location, name);

        this.texture = texture;
        this.signature = signature;

        this.gameProfile = new GameProfile(UUID.randomUUID(), ChatColor.GRAY + "");
        this.gameProfile.getProperties().put("textures", new Property("textures", texture, signature));

        MinecraftServer minecraftServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer worldServer = ((CraftWorld)location.getWorld()).getHandle();

        this.entityPlayer = new EntityPlayer(minecraftServer, worldServer, gameProfile, new PlayerInteractManager(worldServer));
        this.entityPlayer.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    @Override
    public void send(Player player) {
        if (!player.getWorld().equals(getLocation().getWorld())) {
            return;
        }

        if (!player.hasMetadata("NPC") && player.getLocation().distanceSquared(getLocation()) <= SEND_RADIUS_SQUARED) {
            if (!getViewers().contains(player.getUniqueId())) {
                UtilPlayer.sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
                UtilPlayer.sendPacket(player, new PacketPlayOutNamedEntitySpawn(entityPlayer));
                UtilPlayer.sendPacket(player, new PacketPlayOutEntityHeadRotation(entityPlayer, (byte) (entityPlayer.yaw * 256 / 360)));

                getPlugin().getRunnableManager().runTaskLater("bla", metroMC -> UtilPlayer.sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer)), 3);
                getViewers().add(player.getUniqueId());
            }
        } else {
            if (getViewers().contains(player.getUniqueId())) {
                UtilPlayer.sendPacket(player, new PacketPlayOutEntityDestroy(entityPlayer.getId()), new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
                getViewers().remove(player.getUniqueId());
            }
        }
    }

    @Override
    public EntityLiving getEntity() {
        return entityPlayer;
    }
}
