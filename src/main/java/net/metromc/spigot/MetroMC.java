package net.metromc.spigot;

import lombok.Getter;
import net.metromc.services.MongoDBService;
import net.metromc.services.RedisService;
import net.metromc.spigot.command.CommandManager;
import net.metromc.spigot.cooldown.CooldownManager;
import net.metromc.spigot.discord.DiscordBotManager;
import net.metromc.spigot.gui.GuiManager;
import net.metromc.spigot.hologram.HologramManager;
import net.metromc.spigot.network.NetworkManager;
import net.metromc.spigot.npc.NPCManager;
import net.metromc.spigot.punishment.PunishmentManager;
import net.metromc.spigot.redis.RedisHandler;
import net.metromc.spigot.runnable.RunnableManager;
import net.metromc.spigot.scoreboard.ScoreboardManager;
import net.metromc.spigot.story.StoryManager;
import net.metromc.spigot.user.UserManager;
import net.metromc.spigot.user.settings.SettingsManager;
import net.metromc.spigot.world.WorldManager;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class MetroMC extends JavaPlugin {

    @Getter
    private Long start;

    @Getter
    private MongoDBService databaseManager;
    @Getter
    private CommandManager commandManager;
    @Getter
    private RunnableManager runnableManager;
    @Getter
    private WorldManager worldManager;
    @Getter
    private NPCManager npcManager;
    @Getter
    private HologramManager hologramManager;
    @Getter
    private UserManager userManager;
    @Getter
    private RedisHandler redisHandler;
    @Getter
    private ScoreboardManager scoreboardManager;
    @Getter
    private CooldownManager cooldownManager;
    @Getter
    private StoryManager storyManager;
    @Getter
    private NetworkManager networkManager;
    @Getter
    private GuiManager menuManager;
    @Getter
    private PunishmentManager punishmentManager;
    @Getter
    private DiscordBotManager discordBotManager;
    @Getter
    private SettingsManager settingsManager;

    public abstract void enable();
    public abstract void disable();
    
    @Override
    public void onEnable() {
        this.start = System.currentTimeMillis();
        this.databaseManager = new MongoDBService();
        this.commandManager = new CommandManager(this);
        this.runnableManager = new RunnableManager(this);
        this.worldManager = new WorldManager(this);
        this.npcManager = new NPCManager(this);
        this.hologramManager = new HologramManager(this);
        this.userManager = new UserManager(this);
        this.redisHandler = new RedisHandler(new RedisService(getDataFolder()));
        this.scoreboardManager = new ScoreboardManager(this);
        this.cooldownManager = new CooldownManager(this);
        this.storyManager = new StoryManager(this);
        this.networkManager = new NetworkManager(this);
        this.menuManager = new GuiManager(this);
        this.discordBotManager = new DiscordBotManager(this);
        this.punishmentManager = new PunishmentManager(this);
        this.settingsManager = new SettingsManager(this);

        enable();

        redisHandler.subscribe();
    }

    @Override
    public void onDisable() {
        networkManager.unregisterServerInBungee(networkManager.getServerRepository().getThisServer().getName());
        disable();
        // Plugin shutdown logic
    }
}
