package net.metromc.spigot.cooldown.recharge;

import lombok.Getter;
import org.bukkit.entity.Player;

public class RechargeSession {

    @Getter
    private Player player;
    @Getter
    private String element;
    @Getter
    private double cooldown;
    @Getter
    private long time;

    public RechargeSession(Player player, String element, double cooldown) {
        this.player = player;
        this.element = element;
        this.cooldown = cooldown;
        this.time = System.currentTimeMillis();
    }
}
