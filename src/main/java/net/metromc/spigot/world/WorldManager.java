package net.metromc.spigot.world;

import com.google.common.collect.Maps;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.util.Domain;
import net.metromc.spigot.util.ItemBuilder;
import net.metromc.spigot.world.command.CommandRegion;
import net.metromc.spigot.world.command.CommandSpawns;
import net.metromc.spigot.world.listener.RegionListener;
import net.metromc.spigot.world.object.Region;
import net.metromc.spigot.world.object.WorldData;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.inventory.ItemStack;

public class WorldManager extends Module {

    private final RegionListener regionListener;
    private final Map<String, WorldData> worldDatas = Maps.newHashMap();

    public WorldManager(MetroMC plugin) {
        super(plugin, "World Manager");

        registerListener(this.regionListener = new RegionListener(this));
        registerCommand(new CommandRegion(plugin, this), new CommandSpawns(plugin, this));
    }

    @EventHandler
    public void onWorldUnload(WorldUnloadEvent e) {
        this.worldDatas.remove(e.getWorld().getName());
    }

    public WorldData getWorldData() {
        return Bukkit.getWorlds().size() > 0 ? this.getWorldData(Bukkit.getWorlds().get(0)) : null;
    }

    public WorldData getWorldData(World world) {
        return this.getWorldData(world.getName());
    }

    public WorldData getWorldData(String worldName) {
        if (!(new File(worldName).exists())) {
            return null;
        }

        if (!this.worldDatas.containsKey(worldName)) {
            this.worldDatas.put(worldName, new WorldData(worldName));
        }

        return this.worldDatas.get(worldName);
    }

    public ItemStack getRegionWand() {
        return (new ItemBuilder(Material.DIAMOND_HOE).setName(Domain.getDomain().getServerColor() + "Region Wand").build());
    }

    public List<Region> getRegions(Location location) {
        return this.getWorldData(location.getWorld()).getRegions().stream().filter(region -> region.contains(location)).collect(Collectors.toList());
    }

    public Map<String, WorldData> getWorldDatas() {
        return worldDatas;
    }

    public RegionListener getRegionListener() {
        return regionListener;
    }
}
