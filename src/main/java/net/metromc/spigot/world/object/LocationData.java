package net.metromc.spigot.world.object;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class LocationData {

    private final World world;
    private final int x;
    private final int y;
    private final int z;

    public LocationData(World world, Block block) {
        this(world, block.getX(), block.getY(), block.getZ());
    }

    public LocationData(World world, Location location) {
        this(world, location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public LocationData(World world, int x, int y, int z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String toString() {
        return this.world.getName() + "," + this.x + "," + this.y + "," + this.z;
    }

    public String toStringFormatted() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public static LocationData fromString(String string) {
        if (string == null) {
            return null;
        } else {
            String[] array = string.split(",");
            if (array.length != 4) {
                return null;
            } else {
                try {
                    return new LocationData(Bukkit.getWorld(array[0]), Integer.parseInt(array[1]), Integer.parseInt(array[2]),
                            Integer.parseInt(array[3]));
                } catch (NumberFormatException arg2) {
                    return null;
                }
            }
        }
    }

    public Block getBlock(World world) {
        return world.getBlockAt(this.x, this.y, this.z);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
