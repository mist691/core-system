package net.metromc.spigot.world.object;

import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import net.metromc.spigot.util.UtilBlock;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Region {

    private String name;
    private World world;
    private LocationData corner1;
    private LocationData corner2;
    private Consumer<Player> enter;
    private Consumer<Player> leave;

    public Region(String name, World world, LocationData corner1, LocationData corner2) {
        this.name = name;
        this.world = world;
        int minX = corner1.getX() < corner2.getX() ? corner1.getX() : corner2.getX();
        int maxX = corner1.getX() > corner2.getX() ? corner1.getX() : corner2.getX();
        int minY = corner1.getY() < corner2.getY() ? corner1.getY() : corner2.getY();
        int maxY = corner1.getY() > corner2.getY() ? corner1.getY() : corner2.getY();
        int minZ = corner1.getZ() < corner2.getZ() ? corner1.getZ() : corner2.getZ();
        int maxZ = corner1.getZ() > corner2.getZ() ? corner1.getZ() : corner2.getZ();
        this.corner1 = new LocationData(world, minX, minY, minZ);
        this.corner2 = new LocationData(world, maxX, maxY, maxZ);
    }

    public List<Block> getBlocks(World world) {
        return UtilBlock.getBlocks(this.corner1.getBlock(world).getLocation(), this.corner2.getBlock(world).getLocation());
    }

    public String toStringFormatted() {
        return this.name + " " + this.corner1.toStringFormatted() + ", " + this.corner2.toStringFormatted();
    }

    public String toString() {
        return this.world.getName() + ":" + this.name + ":" + this.corner1.toString() + ":" + this.corner2.toString();
    }

    public static Region fromString(String region) {
        String[] splitted = region.split(":");
        if (splitted.length != 4) {
            return null;
        }

        String name = splitted[1];
        LocationData point1 = LocationData.fromString(splitted[2]);
        LocationData point2 = LocationData.fromString(splitted[3]);
        return name != null && point1 != null && point2 != null ? new Region(name, Bukkit.getWorld(splitted[0]), point1, point2) : null;
    }

    public String getName() {
        return name;
    }

    public boolean contains(Entity entity) {
        return this.contains(entity.getLocation());
    }

    public boolean contains(Location loc) {
        int minX = this.corner1.getX();
        int minY = this.corner1.getY();
        int minZ = this.corner1.getZ();
        int maxX = this.corner2.getX();
        int maxY = this.corner2.getY();
        int maxZ = this.corner2.getZ();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        return x >= (double) minX && x <= (double) (maxX + 1) && y >= (double) minY && y <= (double) (maxY + 1)
                && z >= (double) minZ && z <= (double) (maxZ + 1);
    }

    public Collection<Chunk> getChunks(World world) {
        List<Chunk> chunks = Lists.newArrayList();

        int minX = this.corner1.getX();
        int minZ = this.corner1.getZ();
        int maxX = this.corner2.getX();
        int maxZ = this.corner2.getZ();

        for (int x = minX; x <= maxX; ++x) {
            for (int z = minZ; z <= maxZ; ++z) {
                Chunk chunk = world.getChunkAt(new Location(world, (double) x, 0.0D, (double) z));
                if (!chunks.contains(chunk)) {
                    chunks.add(chunk);
                }
            }
        }

        return chunks;
    }

    public Consumer<Player> getEnter() {
        return enter;
    }

    public void setEnter(Consumer<Player> enter) {
        this.enter = enter;
    }

    public Consumer<Player> getLeave() {
        return leave;
    }

    public void setLeave(Consumer<Player> leave) {
        this.leave = leave;
    }
}

