package net.metromc.spigot.story.storys;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.story.object.Story;
import net.metromc.spigot.story.object.StoryReward;
import net.metromc.spigot.util.ItemBuilder;
import org.bukkit.Material;

public class TestStory extends Story {

    public TestStory(MetroMC plugin){
        super("The Beginning", plugin);
    }

    @Override
    public void init() {
//        this.addListener(new EventListener<PlayerJoinEvent>() {
//            @Override
//            public void process(PlayerJoinEvent event) {
//                D.d("Test message join | TestStory");
//            }
//        });
//
//        setFlags(
//                StoryFlag.GODMODE,
//                StoryFlag.MUTE,
//                StoryFlag.NO_INTERACT,
//                StoryFlag.NO_ACTIONS,
//                StoryFlag.NO_MESSAGES
//        );
    }

    @Override
    public boolean complete() {
        return false; //this.getPlayer().getInventory().contains(Material.BEDROCK);
    }

    @Override
    public StoryReward rewards() {
        return new StoryReward(null).addCoins(20.0).addItems(
                new ItemBuilder(Material.DIAMOND, 22).build(),
                new ItemBuilder(Material.EMERALD, 10).build()
        ).addPermissions("essentials.fly");
    }
}
