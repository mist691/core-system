package net.metromc.spigot.story;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.story.object.Story;
import net.metromc.spigot.user.object.User;

public class StoryManager extends Module {

    @Getter
    public Map<User, Story> stories = new HashMap<>();

    public StoryManager(MetroMC plugin) {
        super(plugin, "StoryManager");

        //registerListener(new EventHandlers());
    }

    public Story getStoryFromPlayer(User user) {
        assert stories.containsKey(user);
        return stories.get(user);
    }


}