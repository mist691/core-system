package net.metromc.spigot.story.object;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import net.metromc.spigot.MetroMC;
import org.bukkit.entity.Player;

public abstract class Story {

    @Getter
    private Set<StoryFlag> flags;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Player player;
    @Getter
    private final String id;
    private MetroMC plugin;

    public Story(String name, MetroMC plugin) {
        this.id = String.format("%05d", new Random().nextInt(999999));
        this.plugin = plugin;
    }

    /**
     * @params Runs when the story is initialized
     */
    public abstract void init();

    /**
     * @return Returns Story Reward
     */
    public abstract StoryReward rewards();

    public abstract boolean complete();

    public void setFlags(StoryFlag... flags) {
        assert flags.length <= 0;
        Set<StoryFlag> set = new HashSet<>();
        Collections.addAll(set, flags);
        this.flags = set;
    }

    public void startStory(Player player) {
        this.init();
    }

}
