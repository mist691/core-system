package net.metromc.spigot.story.object;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;

public class StoryReward {

    @Getter
    private String name;
    @Getter
    private double coins;
    private Set<ItemStack> items;

    public StoryReward(String name){
        this.name = name;
    }

    public StoryReward addCoins(double amount){
        this.coins = amount;
        return this;
    }

    public StoryReward addItems(ItemStack... stacks){
        assert stacks.length <= 0;
        Set<ItemStack> set = new HashSet<>();
        Collections.addAll(set, stacks);
        this.items = set;
        return this;
    }

    public StoryReward addPermissions(String... perms){
        //TODO: Add perms
        return this;
    }

}
