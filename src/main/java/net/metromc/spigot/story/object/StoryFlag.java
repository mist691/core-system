package net.metromc.spigot.story.object;

public enum StoryFlag {

    GOD_MODE, NO_INTERACT, MUTE, NO_MESSAGES, NO_MOVEMENT, NO_ACTIONS, SPECTATE, DISABLE_EVERYTHING

}
