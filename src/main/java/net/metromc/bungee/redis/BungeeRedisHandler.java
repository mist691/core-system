package net.metromc.bungee.redis;

import com.google.common.base.Supplier;
import net.md_5.bungee.api.plugin.Event;
import net.metromc.bungee.MetroMcBungee;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.services.RedisService;
import net.metromc.services.redis.MultiChannelRedisModule;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class BungeeRedisHandler extends MultiChannelRedisModule {

    private final MetroMcBungee plugin;

    private final String host;
    private final int port;

    public BungeeRedisHandler(RedisService service, MetroMcBungee plugin) {
        super(service);
        this.plugin = plugin;

        this.host = service.getRepository().getHost();
        this.port = service.getRepository().getPort();

        registerChannel(ServerDataRepository.UNREGISTER_SERVER_CHANNEL, ServerDataRepository.REGISTER_SERVER_CHANNEL);
    }

    @Override
    public void recieve(String channel, String message) {
        Event event = new RedisMessageEvent(channel, message);

        plugin.getProxy().getPluginManager().callEvent(event);
    }

    public void removeData(String key) {
        new Thread(() -> {
            System.out.println("Jedis remove date");
            Jedis jedis = new Jedis(host, port);
            System.out.println("Jedis deleting data");
            jedis.hdel("metromc", key);
            System.out.println("Jedis deleted data");
            jedis.close();
            System.out.println("Jedis closed");
        }).run();
    }

    public Supplier<String> getData(String key) {
        Jedis jedis = new Jedis(host, port);

        Supplier<String> supplier = () -> jedis.hget("metromc", key) == null ? null : new String(jedis.hget("metromc", key));

        jedis.close();

        return supplier;
    }
}
