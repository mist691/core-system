#!/bin/bash

name=\$1
server_type=\$2
port=\$3
min_ram=\$4
max_ram=\$5

new_server=/home/network/servers/\$name

sh /home/network/scripts/destroy-server.sh \$name \$port

cp -r /home/network/server-template/. \$new_server/
cp -r /home/network/server-types-template/\$server_type/. \$new_server/

cd \$new_server

tmux new-session -d -s \$name java -Dgametype=\$2 -Dname=\$1 -Xms"\$min_ram"M -Xmx"\$max_ram"M  -Ddbport="27017" -Ddbname="admin" -Ddbuser="admin" -Dserver="METRO_MC" -Ddbpassword="FqL1XYIm" -jar spigot.jar -o true -s 2000 -port \$port